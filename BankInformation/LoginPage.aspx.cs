﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BankInformation
{
	public partial class LoginPage : System.Web.UI.Page
	{
		string connentionstring = @"Server=SHAKIL;Database=Bankdb;User id=sa;Password=123";
		protected void Page_Load(object sender, EventArgs e)
		{
			lblerrormessage.Visible = false;
		}

		protected void loginbtn_Click(object sender, EventArgs e)
		{
			SqlConnection connection = new SqlConnection(connentionstring);

			string query = "SELECT count(1) from Users where userName=@username and password=@password";

			SqlCommand command = new SqlCommand(query,connection);
			connection.Open();
			command.Parameters.AddWithValue("@username", txtUsername.Text.Trim());
			command.Parameters.AddWithValue("@password", txtPassword.Text.Trim());
		
			int count = Convert.ToInt32(command.ExecuteScalar());
			if (count == 1)
			{
				Session.Contents.RemoveAll();
				Session["username"] = txtUsername.Text.Trim();
				Response.Redirect("HomePage.aspx");
			}
			else
			{
				lblerrormessage.Visible = true;
			}
		}
	}
}