﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Transfer.aspx.cs" Inherits="BankInformation.Transfer" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
<style>
		body 
		{
			background-color:wheat;
		}
		</style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
			 <table style="margin:auto;border:5px solid white">
                <tr>
                    <td></td>
                    <td>
                        <asp:Label ID="Label1" runat="server" Text="Transfer"></asp:Label>
                    </td>
                </tr>
                  <tr>
                    <td>
                         <asp:Label ID="Label2" runat="server" Text="From Account"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtFromAccount" runat="server"></asp:TextBox>
                    </td>
                </tr>
                 <tr>
                    <td>
                         <asp:Label ID="Label3" runat="server" Text="To Account"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtToAccount" runat="server"></asp:TextBox>
                    </td>
                </tr>
				    <tr>
                    <td>
                         <asp:Label ID="Label4" runat="server" Text="Amount"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtamount" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <asp:Button ID="savebutton" runat="server" Text="Save" OnClick="savebutton_Click" />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <br />
                        <br />
                        <br />
                        <br />
                        <asp:Label ID="messagelabel" runat="server"></asp:Label>
                        <br />
                        <br />
                        <br />
                        <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click">Back to Home</asp:LinkButton>

                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
