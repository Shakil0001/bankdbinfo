﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LoginPage.aspx.cs" Inherits="BankInformation.LoginPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
	<style>
		body 
		{
			background-color:wheat;
		}
	</style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
			<table style="margin:auto;border:5px solid white">
				<tr forecolor="#FF99FF">
					<td>

					</td>
					<td>
						<asp:Label ID="Label3" runat="server" Text="Login"></asp:Label>
					</td>
				</tr>
				<tr>
					<td>
						<asp:Label ID="Label1" runat="server" Text="UserName"></asp:Label>
					</td>
					<td>
						<asp:TextBox ID="txtUsername" runat="server"></asp:TextBox>
					</td>
				</tr>
					<tr>
					<td>
						<asp:Label ID="Label2" runat="server"  Text="Password"></asp:Label>
					</td>
					<td>
						<asp:TextBox ID="txtPassword" runat="server" TextMode="Password"></asp:TextBox>
					</td>
				</tr>
				<tr>
					<td></td>
					<td>
						<asp:Button ID="loginbtn" runat="server" Text="Login" OnClick="loginbtn_Click" />
					</td>
				</tr>
				<tr>
					<td>
						<asp:Label ID="lblerrormessage" runat="server" Text="Incorrect User Credentials " ForeColor="Red"></asp:Label>
					</td>
				</tr>
			</table>
        </div>
    </form>
</body>
</html>
