﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BankInformation
{
    public partial class Transfer : System.Web.UI.Page
    {
        string connentionstring = @"Server=SHAKIL;Database=Bankdb;User id=sa;Password=123";
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            Session.Abandon();
            Response.Redirect("HomePage.aspx");
        }

        protected void savebutton_Click(object sender, EventArgs e)
        {
            double amt;
            using (var con = new SqlConnection(connentionstring))
            {
                var sql = "select Balance from Accounts WHERE accountNumber = @AccountNumber";
                using (var cmd = new SqlCommand(sql, con))
                {
                    cmd.Parameters.AddWithValue("@AccountNumber", (string)(txtFromAccount.Text));

                    con.Open();
                    amt = (double)cmd.ExecuteScalar();
                    var diff = amt - Convert.ToDouble(txtamount.Text);

                    if (diff > 500)
                    {

                        SqlConnection connection = new SqlConnection(connentionstring);
                        string query = "Update Accounts set balance = balance - @Balance where accountnumber = @FromAccountNumber   Update Accounts set balance = balance + @Balance where accountnumber = @ToAccountNumber";
                        SqlCommand command = new SqlCommand(query, connection);
                        command.Parameters.AddWithValue("@FromAccountNumber", txtFromAccount.Text.Trim());
                        command.Parameters.AddWithValue("@ToAccountNumber", txtToAccount.Text.Trim());
                        command.Parameters.AddWithValue("@Balance", txtamount.Text.Trim());
                        connection.Open();
                        command.ExecuteNonQuery();
                        connection.Close();
                        messagelabel.Text = "Transfer Successfully";
                    }
                    else
                    {
                        messagelabel.Text = "Account Balance must be 500 and more.";
                    }
                }
            }
        }
    }
}