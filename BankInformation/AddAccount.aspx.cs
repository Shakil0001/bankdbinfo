﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BankInformation
{
    public partial class AddAccount : System.Web.UI.Page
    {
        string connentionstring = @"Server=SHAKIL;Database=Bankdb;User id=sa;Password=123";
		protected void Page_Load(object sender, EventArgs e)
        {            
        }
        protected void btnsaveaccount_Click(object sender, EventArgs e)
        {
			DateTime systemDate = DateTime.Now;
			SqlConnection connection = new SqlConnection(connentionstring);
			string amt;
			double amount = 0.0;
			amount = Convert.ToDouble(txtbalance.Text);

			if (amount < 500)
			{
				messagelabel.Text = "Account opening Balance must 500 and more. ";
			}
			else
			{

				using (var con = new SqlConnection(connentionstring))
				{
					var sql = "select AccountNumber from Accounts WHERE AccountNumber = @AccountNumber";
					using (var cmd = new SqlCommand(sql, con))
					{
						cmd.Parameters.AddWithValue("@AccountNumber", (string)(txtAccountNo.Text));
						con.Open();
						amt = (string)cmd.ExecuteScalar();

						if (amt != txtAccountNo.Text)
						{
							string query = "insert into [dbo].[Accounts](accountNumber,accountName,balance,accountType,openingdate,lastTransaction)values(@AccountNumber,@AccountName,@Balance,@AccountType,@OpeningDate,@LastTransaction)";
							SqlCommand command = new SqlCommand(query, connection);
							command.Parameters.AddWithValue("@AccountNumber", txtAccountNo.Text.Trim());
							command.Parameters.AddWithValue("@AccountName", txtAccountName.Text.Trim());
							command.Parameters.AddWithValue("@Balance", txtbalance.Text.Trim());
							command.Parameters.AddWithValue("@AccountType", dropdownlistaccountType.SelectedValue.ToString());
							command.Parameters.AddWithValue("@OpeningDate", systemDate);
							command.Parameters.AddWithValue("@LastTransaction", systemDate);
							connection.Open();
							command.ExecuteNonQuery();
							connection.Close();
                            messagelabel.Text = "";

						}
                        else
                        {
                            messagelabel.Text = "AccountNumber Is already Exist";

                        }

                    }
				}
			}
			
		}

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
			Session.Abandon();
            Response.Redirect("HomePage.aspx");
        }
    }
}