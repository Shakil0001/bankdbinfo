﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BankInformation
{
	public partial class HomePage : System.Web.UI.Page
	{
		string connentionstring = @"Server=SHAKIL;Database=Bankdb;User id=sa;Password=123";
		protected void Page_Load(object sender, EventArgs e)
		{
            if (!IsPostBack)
            {
                populateGridview();
            }
		}
        void populateGridview()
        {
            DataTable dtbl = new DataTable();
            using (SqlConnection connection = new SqlConnection(connentionstring))
            {
                connection.Open();
                SqlDataAdapter sqlda = new SqlDataAdapter("Select * from Accounts ", connection);
                sqlda.Fill(dtbl);

            } 
            if (dtbl.Rows.Count > 0)
            {
                gvAccounts.DataSource = dtbl;
                gvAccounts.DataBind();
            }
            else
            {
                dtbl.Rows.Add(dtbl.NewRow());
                gvAccounts.DataSource = dtbl;
                gvAccounts.DataBind();
                gvAccounts.Rows[0].Cells.Clear();
                gvAccounts.Rows[0].Cells.Add(new TableCell());
                gvAccounts.Rows[0].Cells[0].ColumnSpan = dtbl.Columns.Count;
                gvAccounts.Rows[0].Cells[0].Text = "no data found";
                gvAccounts.Rows[0].Cells[0].HorizontalAlign = HorizontalAlign.Center;

            }
        }
        protected void LinkButton5_Click(object sender, EventArgs e)
		{
			Session.Contents.RemoveAll();
			Session.Abandon();
			Response.Redirect("LoginPage.aspx");
		}

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
			Session.Contents.RemoveAll();
			Session.Abandon();
            Response.Redirect("AddAccount.aspx");
        }

        protected void LinkButton2_Click(object sender, EventArgs e)
        {
			Session.Contents.RemoveAll();
			Session.Abandon();
            Response.Redirect("Deposit.aspx");
        }

		protected void LinkButton3_Click(object sender, EventArgs e)
		{
			Session.Contents.RemoveAll();
			Session.Abandon();
			Response.Redirect("Withdraw.aspx");
		}

		protected void LinkButton4_Click(object sender, EventArgs e)
		{
			Session.Contents.RemoveAll();
			Session.Abandon();
			Response.Redirect("Transfer.aspx");
		}

        protected void gvAccounts_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            
                try
                {
                    using (SqlConnection connection = new SqlConnection(connentionstring))
                    {
                        connection.Open();
                        string query = "Delete from Accounts  where AccountID=@accountID";
                        SqlCommand command = new SqlCommand(query, connection);
                        command.Parameters.AddWithValue("@accountID", Convert.ToInt32(gvAccounts.DataKeys[e.RowIndex].Value.ToString()));
                        command.ExecuteNonQuery();					    
                        populateGridview();
                        lblsuccessmessage.Text = "Delete successfully";

                    }

                }
                catch (Exception ex)
                {
                    lblsuccessmessage.Text = "";
                    lblerromessage.Text = ex.Message;
                }


            }
        }
    }
