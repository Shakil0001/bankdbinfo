﻿using System.Data.SqlClient;

namespace BankInformation
{
    internal class Sqlcommand
    {
        private string query;
        private SqlConnection connection;

        public Sqlcommand(string query, SqlConnection connection)
        {
            this.query = query;
            this.connection = connection;
        }
    }
}