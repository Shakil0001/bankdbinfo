﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddAccount.aspx.cs" Inherits="BankInformation.AddAccount" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>
		body 
		{
			background-color:wheat;
		}
	</style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table style="margin:auto;border:5px solid white">
				<tr>
					<td></td>
					<td>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<asp:Label ID="Label4" runat="server" Text="AddNew"></asp:Label>
					</td>
				</tr>
                <tr>
                    <td>
                        <asp:Label ID="Label1" runat="server" Text="AccountNo"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtAccountNo" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorAccountNo" runat="server"
                            ErrorMessage="AccountNumber Cann't be Empty" ForeColor="#ff0000" ControlToValidate="txtAccountNo" Display="Dynamic"></asp:RequiredFieldValidator>
                    </td>			
                </tr>
                   <tr>
                    <td>
                        <asp:Label ID="Label2" runat="server" Text="AccountName"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtAccountName" runat="server"></asp:TextBox>
                    </td>
                </tr>
				  <tr>
                    <td>
                        <asp:Label ID="Label3" runat="server" Text="Balance"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtbalance" runat="server"></asp:TextBox>
                    </td>
                </tr>	 
				<tr>
					<td>
						 <asp:Label ID="Label5" runat="server" Text="AccountType"></asp:Label>
					</td>
					<td>
						<asp:DropDownList ID="dropdownlistaccountType" runat="server">
							<asp:ListItem Value="Savings">Savings</asp:ListItem>
							<asp:ListItem Value="Credit">Credit</asp:ListItem>
							<asp:ListItem Value="Current">Current</asp:ListItem>
							<asp:ListItem Value="Fixed">Fixed</asp:ListItem>
							<asp:ListItem></asp:ListItem>
						</asp:DropDownList>
					</td>
				</tr>
                <tr>
                    <td></td>
                    <td>
                        <asp:Button ID="btnsaveaccount" runat="server" Text="SaveAccount" OnClick="btnsaveaccount_Click" />
                    </td>
                </tr>
                <tr>
                   <td></td>
                    <td> 
						<asp:Label ID="messagelabel" runat="server"  ForeColor="#cc0000" ></asp:Label>
					<td>
                        <br />
                        <br />
                        <br />
                        <asp:LinkButton ID="LinkButton1" runat="server" OnClick="LinkButton1_Click" CausesValidation="false">Back to Home</asp:LinkButton></td></td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
